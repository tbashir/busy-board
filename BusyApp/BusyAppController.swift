//
//  BusyAppController.swift
//  BusyApp
//
//  Created by Tanveer Bashir on 5/9/15.
//  Copyright (c) 2015 Tanveer Bashir. All rights reserved.
//

import Cocoa
import AppKit

class BusyAppController: NSWindowController {

    @IBOutlet weak var checkBoxLabel: NSButton!
    @IBOutlet weak var secureTextField: NSSecureTextField!
    @IBOutlet weak var revealTextField: NSTextField!
    @IBOutlet weak var upDownLabel: NSTextField!
    @IBOutlet weak var slider: NSSlider!
    @IBOutlet weak var showTick: NSButton!
    @IBOutlet weak var hideTick: NSButton!
    var sliderValue:Double = 0

    override var windowNibName:String? {
        return "BusyAppWindow"
    }

    override func windowDidLoad() {
        super.windowDidLoad()
        window?.backgroundColor = NSColor(calibratedRed: 45/255, green: 75/255, blue: 125/255, alpha: 1.0)
        upDownLabel.textColor = NSColor.whiteColor()
    }

    @IBAction func checkBox(sender: NSButton) {
        switch sender.state{
        case NSOffState:
            checkBoxLabel.title = "Check me"
        case NSOnState:
            checkBoxLabel.title = "Uncheck me"
        default:
            checkBoxLabel.title = "Check me"
        }
    }

    func readSliderMovementState(state: Double) -> String{
        if state > sliderValue {
            return "Going up!"
        } else if state < sliderValue {
            return "Going down!"
        } else {
            return "Take a break!"
        }
    }

    @IBAction func upDownSlider(sender: NSSlider) {
        let currentValue = sender.doubleValue
        upDownLabel.stringValue = readSliderMovementState(currentValue)
        sliderValue = currentValue
    }

    @IBAction func showHideSliderTick(sender: NSButton) {
        var num = sender.tag
        var hideState = hideTick.state
        if num == NSOffState && hideState == NSOffState {
            slider.numberOfTickMarks = Int(slider.maxValue)
        } else {
            slider.numberOfTickMarks = 0
        }
    }

    @IBAction func revealSecretText(sender: NSButton) {
        var mesg = secureTextField.stringValue
        revealTextField.stringValue = mesg
    }

    @IBAction func resetControls(sender: NSButton) {
        showTick.state = NSOffState
        hideTick.state = NSOffState
        secureTextField.stringValue = ""
        revealTextField.stringValue = ""
        slider.numberOfTickMarks = 0
        checkBoxLabel.state = NSOffState
        checkBoxLabel.title = "Check me"
        upDownLabel.stringValue = ""
    }
}
