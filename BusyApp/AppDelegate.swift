//
//  AppDelegate.swift
//  BusyApp
//
//  Created by Tanveer Bashir on 5/9/15.
//  Copyright (c) 2015 Tanveer Bashir. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var busyAppWindow: BusyAppController!
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        let busyAppWindow = BusyAppController()
        busyAppWindow.showWindow(self)
        self.busyAppWindow = busyAppWindow
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

